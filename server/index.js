// Set requirements
const express = require("express");
const mysql = require("mysql");
const cors = require("cors");

const app = express();

// Include .env
require('dotenv').config();

// Enable cors to be able to make requests to the front-end
app.use(cors());

// Enable json to receive the body from the front-end
app.use(express.json());

// Set up database
const db = mysql.createConnection({
    user: "root",
    host: "localhost",
    password: "Test#1234",
    database: "employeedb",
});

// Make a request to the client side to receive the values below
app.post("/create", (req, res) => {
    const name = req.body.name;
    const position = req.body.position;
    const experience = req.body.experience;
    const salary = req.body.salary;

// Query to add an employee
db.query(
"INSERT INTO employees (name, position, experience, salary) VALUES (?,?,?,?)",
[name, position, experience, salary],
(err, result) => {
    if (err) {
    console.log(err);
    } else {
    res.send("Values Inserted");
    }
}
);
});

// Query to list all the employees
app.get("/employees", (req, res) => {
  db.query("SELECT * FROM employees", (err, result) => {
    if (err) {
        console.log(err);
    } else {
        res.send(result);
    }
});
});

// Query to update an employee
app.put("/update", (req, res) => {
    const id = req.body.id;
    const salary = req.body.salary;
    db.query(
        "UPDATE employees SET salary = ? WHERE id = ?",
        [salary, id],
        (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.send(result);
        }
        }
    );
});

// Query to remove an employee
app.delete("/delete/:id", (req, res) => {
    const id = req.params.id;
    db.query("DELETE FROM employees WHERE id = ?", id, (err, result) => {
    if (err) {
        console.log(err);
    } else {
        res.send(result);
    }
    });
});

// Port which the app will listen on
// Access PORT from environment variables
const PORT = process.env.PORT || 3001;

// Listen to the specified port
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});
