// Sample attempt at how routes work

// Handle the functions that will be called when the user navigates through the system
const express = require('express');

router = express.Router();
initial = require('../controllers/initial');

// Call the function hello.hello when the user's at '/'
router.get('/', initial.hello);

module.exports = router;